function Library(bookshelf, clients, RentLog) {
    this.bookshelf = bookshelf;
    this.clients = clients;
    this.RentLog=RentLog;
    this.showAllBooks = function(){
        let books=this.bookshelf.listOfBooks;
        document.querySelector('#info').innerHTML ="  Here is the list of all books we can offer :"
        let booksDiv = document.querySelector('#table');
        booksDiv.innerHTML="";
        booksDiv.innerHTML = `
        <table class="table  table-striped">
        <thead class="font-weight-bold">
        <tr class="font-weight-bold"> <td>Book ID</td><td>Book name</td><td>Book author</td> <td>Pcs available</td><td>Action</td></tr>
        </thead>
    `;
    books.forEach((book) => {
        booksDiv.innerHTML += `
                <tr>
                    <td>${book.id}</td>
                    <td>${book.name}</td>
                    <td>${book.author}</td>
                    <td>${book.noAvailable}</td>
                    <td><button type="submit" value="${book.id}" onclick="return library.rentBook(${localStorage.id},${book.id});" >Take it!</button></td>
                </tr>
        `;
    });
   
    }
    this.showClients = function(){
        let clients=this.clients.listOfClients;
        document.querySelector('#info').innerHTML =" Last activity log :"
        let clientsTable = document.querySelector('#table');
        clientsTable.innerHTML="";
        clientsTable.innerHTML = `
        <table class="table  table-striped">
        <thead class="font-weight-bold">
        <tr class="font-weight-bold"> <td>Client ID</td><td>Username</td><td>Phone</td> <td>E-mail</td><td>Books borrowed</td></tr>
        </thead>
    `;
    clientsTable.innerHTML += `
    <tr>
                    
                    <td><input class="form-control" type="text" id="name"  placeholder="new user name"></td>
                    <td><input class="form-control" type="tel" id="phone"  placeholder="new phone number"></td>
                    <td><input class="form-control" type="email" id="email"  placeholder="new email"></td>
                    <td><button type="submit" onclick="return library.registerUser()" >Register a new user!</button></td>
                    
                </tr>` ;
    clients.forEach((client) => {
        clientsTable.innerHTML += `
                <tr>
                    <td>${client.id}</td>
                    <td>${client.name}</td>
                    <td>${client.phone}</td>
                    <td>${client.email}</td>
                    <td>${client.books}</td>
                    
                </tr>
        `;
    });
  
    }
    this.showReport = function(){
        let report=this.RentLog.rentalList;
        document.querySelector('#info').innerHTML =" Last activity log :"
        let activityTable = document.querySelector('#table');
        activityTable.innerHTML="";
        activityTable.innerHTML = `
        <table class="table  table-striped">
        <thead class="font-weight-bold">
        <tr class="font-weight-bold"> <td>Client ID</td><td>Book ID</td><td>Status</td> <td>Date</td></tr>
        </thead>
    `;
    report.forEach((record) => {
        activityTable.innerHTML += `
                <tr>
                    <td>${record.clientId}</td>
                    <td>${record.bookId}</td>
                    <td>${record.status}</td>
                    <td>${record.date}</td>
                    
                </tr>
        `;
    });
        console.table(this.RentLog.rentalList);
    }
    this.rentBook = function(clientId, bookId, date){
      //rent a book//
      var clientIndex=this.clients.findClient(clientId);
      var bookIndex=this.bookshelf.findBook(bookId);
      if(clientIndex==null){alert("Wrong user ID!");
    }else if(bookIndex==null){alert("Wrong book ID!")}else{
      var client=this.clients.listOfClients[clientIndex];
      var book=this.bookshelf.listOfBooks[bookIndex];
      if (book.noAvailable>0){
      client.books.forEach((book) => {
          if (book.id==bookId){
              alert("Sorry you have already taken this book! You can take only one copy of each book.")
              throw "error, cannot rent tow copies!"
          }
      });
       if(client.books.length<5){
        client.books.push(book)
         book.noAvailable -= 1;
     this.bookshelf.updateBook(bookIndex, book);
     this.RentLog.add(new LogRecord(clientId,bookId,"rented")); 
     this.showAllBooks();
     alert("Book borrowed!")
      } else {alert("Sorry, you can't take more than 5 books, you need to return book first!")}
       }else alert("Sorry, this book is unavailable at the moment!")   
    }    
    this.returnBook = function(clientId, bookId, date){
        //return a book//
        var clientIndex=this.clients.findClient(clientId);
      var bookIndex=this.bookshelf.findBook(bookId);
      if(clientIndex==null){console.log("Wrong user ID!");
    }else if(bookIndex==null){console.log("Wrong book ID!")}else{
      var client=this.clients.listOfClients[clientIndex];
      var book=this.bookshelf.listOfBooks[bookIndex];
        book.noAvailable += 1;
        this.bookshelf.updateBook(book);
        this.RentLog.add(new LogRecord(clientId,bookId,"returned"));
        for (let i = 0; i < client.books.length; i++) {
            if(client.books[i].id==bookId){
                client.books.splice(i,1); }
        } this.showMyBooks(client.id);
            
        }
    }
}
    this.showMyBooks = function(clientId){
        document.querySelector('#info').innerHTML ="You can borrow up to 5 books.  Here is the list of books you borrowed:"
        let booksDiv = document.querySelector('#table');
        booksDiv.innerHTML="";
    booksDiv.innerHTML = `
        <table class="table  table-striped">
        <thead >
        <tr class="font-weight-bold"><td>Book ID</td> <td>Book name</td><td>Book author</td> <td>Action</td></tr>
        </thead>
    `;
        var clientIndex=this.clients.findClient(clientId);
        let mybooks=this.clients.listOfClients[clientIndex].books
       
    if (mybooks.length>0){
        mybooks.forEach((book) => {
        booksDiv.innerHTML += `
                <tr>
                    <td>${book.id}</td>
                    <td>${book.name}</td>
                    <td>${book.author}</td>
                    <td><button type="submit" value="${book.id}" onclick="return library.returnBook(${localStorage.id},${book.id});" >Return it!</button></td>
                </tr>
        `;
    });}else{ booksDiv.innerHTML += `<tr>Nothing borrowed yet </tr>`}
    booksDiv.innerHTML += "</table>";
      

    }
    this.registerUser = function(){
        let name=document.querySelector('#name').value;
        let phone=document.querySelector('#phone').value;
        let email=document.querySelector('#email').value;
        console.log(name+phone+email);
        this.clients.add(new Client(name, phone, email));
        this.showClients();

    }

    this.showAllBooksAdmin = function(){
        let books=this.bookshelf.listOfBooks;
        document.querySelector('#info').innerHTML ="  Here is the list of all books :"
        let booksDiv = document.querySelector('#table');
        booksDiv.innerHTML="";
        booksDiv.innerHTML = `
        <table class="table  table-striped">
        <thead class="font-weight-bold">
        <tr class="font-weight-bold"> <td>Book ID</td><td>Book name</td><td>Book author</td> <td>Pcs available</td></tr>
        </thead>
    `;
    booksDiv.innerHTML += `
    <tr>
                    
                    <td><input class="form-control" type="text" id="bName"  placeholder="new book name"></td>
                    <td><input class="form-control" type="text" id="authName"  placeholder="Author name"></td>
                    <td><input class="form-control" type="number" id="pscs"  placeholder="pcs available"></td>
                    <td><button type="submit" onclick="return library.addBook()" >Add a new book!</button></td>
                    
                </tr>` ;
    books.forEach((book) => {
        booksDiv.innerHTML += `
                <tr>
                    <td>${book.id}</td>
                    <td>${book.name}</td>
                    <td>${book.author}</td>
                    <td>${book.noAvailable}</td>
                </tr>
        `;
    });
      
    }
    this.addBook = function(){
 
        let name=document.querySelector('#bName').value;
        let author=document.querySelector('#authName').value;
        let noAvailable=document.querySelector('#pscs').value;
        this. bookshelf.add (new Book(name, author, noAvailable));
        this.showAllBooksAdmin();

    }
}