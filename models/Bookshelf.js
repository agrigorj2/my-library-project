function Bookshelf (books){
    this.listOfBooks = books ||[];
    let arr=this.listOfBooks;
    this.add = function(book){
        this.listOfBooks.push(book);
    }
    this.findBook = function (id) {
        // find the book
        for (let i = 0; i < arr.length; i++) {
            if (arr[i].id==id){
                console.log(arr[i]);
                return i;
            }
            
        }
    }

    this.updateBook = function(id,book){
        // find and update
        this.listOfBooks[id]=book;
    }
}

function Book(name, author, noAvailable){
    this.id = generateRandomID();
    this.name = name;
    this.author = author;
    this.noAvailable = noAvailable;
    this.reduceNoAvailable = function (noBooksTaken) {
        this.noAvailable -= noBooksTaken;
    }
    this.updateBook = function(book){
        // find and update
    }
}