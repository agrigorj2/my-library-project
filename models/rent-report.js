function RentLog() {
    this.rentalList = [];
    this.add = function(LogRecord){
        this.rentalList.push(LogRecord);
    }
}

function LogRecord(clientId, bookId, status, date) {
    this.clientId = clientId;
    this.bookId = bookId;
    this.status=status;
    this.date = date || new Date();
}